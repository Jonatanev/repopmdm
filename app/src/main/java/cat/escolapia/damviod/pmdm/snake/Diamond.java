package cat.escolapia.damviod.pmdm.snake;

public class Diamond {
    public static final int TYPE_1 = 0;
    public static final int TYPE_2 = 1;
    public int x, y;
    public int type;

    public Diamond(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }
    public Diamond()
    {
        this.x = -1;
        this.y = -1;
        this.type = 1;
    }
}
