package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;
import cat.escolapia.damviod.pmdm.framework.Sound;
import cat.escolapia.damviod.pmdm.framework.Music;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.png", PixmapFormat.RGB565);
        Assets.background2 = g.newPixmap("Background2.png", PixmapFormat.RGB565);
        Assets.background3 = g.newPixmap("background3.png", PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("logo.png", PixmapFormat.ARGB4444);
        Assets.mainMenu = g.newPixmap("mainmenu.png", PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("numbers.png", PixmapFormat.ARGB4444);
        Assets.ready = g.newPixmap("ready.png", PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pausemenu.png", PixmapFormat.ARGB4444);
        Assets.gameOver = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.headUp = g.newPixmap("headup.png", PixmapFormat.ARGB4444);
        Assets.headLeft = g.newPixmap("headleft.png", PixmapFormat.ARGB4444);
        Assets.headDown = g.newPixmap("headdown.png", PixmapFormat.ARGB4444);
        Assets.headRight = g.newPixmap("headright.png", PixmapFormat.ARGB4444);
        Assets.tail = g.newPixmap("tail.png", PixmapFormat.ARGB4444);
        Assets.diamond = g.newPixmap("diamond.png", PixmapFormat.ARGB4444);
        Assets.manzana = g.newPixmap("manzana.png", PixmapFormat.ARGB4444);
        Assets.wall = g.newPixmap("wall.png", PixmapFormat.ARGB4444);
        Assets.Credits= g.newPixmap("Credits.png", PixmapFormat.ARGB4444);
        Assets.click = game.getAudio().newSound("click.wav");
        Assets.eat = game.getAudio().newSound("eat.ogg");
        Assets.xoc = game.getAudio().newSound("bitten.ogg");
        Assets.musicMenu = game.getAudio().newMusic("menu.mp3");
        Assets.musicGame = game.getAudio().newMusic("mario64theme.mp3");
        Assets.musicTurbo = game.getAudio().newMusic("powerfulmario.mp3");


        Settings.load(game.getFileIO());

        game.setScreen(new MainMenuScreen(game));

    }
    
    public void render(float deltaTime) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}
