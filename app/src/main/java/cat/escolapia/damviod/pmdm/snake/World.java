package cat.escolapia.damviod.pmdm.snake;

import android.util.Log;
import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;
import java.util.Random;

import static cat.escolapia.damviod.pmdm.snake.Assets.background2;


public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.05f;

    public Snake snake;
    public Diamond diamond;
    public Diamond diamond2;
    public Diamond manzana;
    public Wall walls;
    public boolean gameOver = false;;
    public int score = 0;
    public int scoretick=0; //augmento velocidad

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;
    int manzanar=0;
    float accumulator = 0.0f;
    boolean turbo=false;
    boolean vel=false;

    public World( ) {

        snake = new Snake();
        walls=createWalls();
        diamond=placeDiamond();
        diamond2=placeDiamond();
        manzana=new Diamond();
    }

    private Wall createWalls()
    {
        Wall w;
        int wallX = WORLD_WIDTH/2;
        int wallY = WORLD_HEIGHT/2;
        w = new Wall(wallX,wallY);
        return w;
    }
    private Diamond placeDiamond()
    {
        Diamond d;
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }

        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;
            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
            //Comprobar que no ponga diamante en Wall
            if(diamondX==walls.x && (diamondY>=walls.y && diamondY<=walls.y+4))
            {
                diamondX+=1;
                if (diamondX >= WORLD_WIDTH) {
                    diamondX = 0;
                    diamondY += 1;
                    if (diamondY >= WORLD_HEIGHT) {
                        diamondY = 0;
                    }
                }
            }
        }

         d = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
        return d;
    }
    private Diamond placeManzana()
    {
        Diamond d;
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }
        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }
        int manzanaX = random.nextInt(WORLD_WIDTH);
        int manzanaY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[manzanaX][manzanaY] == false) break;
            manzanaX += 1;
            if (manzanaX >= WORLD_WIDTH) {
                manzanaX = 0;
                manzanaY += 1;
                if (manzanaY >= WORLD_HEIGHT) {
                    manzanaY = 0;
                }
            }
            //Comprobar que no ponga diamante en Wall
            if(manzanaX==walls.x && (manzanaY>=walls.y && manzanaY<=walls.y+4))
            {
                manzanaX+=1;
                if (manzanaX >= WORLD_WIDTH) {
                    manzanaX = 0;
                    manzanaY += 1;
                    if (manzanaY >= WORLD_HEIGHT) {
                        manzanaY = 0;
                    }
                }
            }
        }
        d = new Diamond(manzanaX, manzanaY, Diamond.TYPE_2);
        return d;
    }

    public void update(float deltaTime) {
        if (gameOver) return;
        tickTime += deltaTime;
        //vel=false;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();

            /*****Cuando come una manzana*****/
            if(turbo) modeTurboOff();

            /******Comprovaciones de colisiones******/
            Colisiones();
            /*****Fin Colisiones*****/

            /*****Velocidad por Puntuaciones*****/
            scoreVelocidad();
            /***Fin Vel. Scores****/
        }
    }
    public void Colisiones()
    {
        if (snake.checkXoca()) {
            gameOver = true;
            return;
        }
        if(snake.checkXocWall(walls)) {
            gameOver = true;
            return;
        }
        if(snake.checkXocDiamant(diamond)) // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
        {
            score+=SCORE_INCREMENT;
            snake.allarga();
            ////Audios////
            Assets.eat.play(1);
            diamond=placeDiamond();
            manzanar=random.nextInt(9);
            if(manzanar==5){
                manzana=placeManzana();
            }
            vel=true;
        }
        if(snake.checkXocDiamant(diamond2))
        {
            score+=SCORE_INCREMENT;
            snake.allarga();
            ////Audios////
            Assets.eat.play(1);
            diamond2=placeDiamond();
            manzanar=random.nextInt(10);
            if(manzanar==5){
                manzana=placeManzana();
            }
            vel=true;
        }
        if(snake.checkXocManzana(manzana))
        {
            //Audios//
            Assets.eat.play(1);
            manzana.x=-1;
            manzana.y=-1;
            modeTurboOn();
        }
    }

    public void scoreVelocidad()
    {
        if(score==30 && !vel )
        {
            tick-=TICK_DECREMENT; // baixar el tick per augmentar la velocitat de joc
            vel=true;
            cambiaBackg(1);
        }
        if(score==60 && !vel )
        {
            tick-=TICK_DECREMENT;
            vel=true;
            cambiaBackg(2);
        }
    }
    /*****MODO TURBO****/
    public void modeTurboOn()
    {
        turbo=true;
        accumulator=4f;
        tick=0.2f;
        //Audios//
        Assets.musicGame.stop();
        Assets.musicTurbo.play();
    }
    public void modeTurboOff()
    {
        accumulator-=tick;
        Log.d("acumulador ",Float.toString(accumulator));
        if(accumulator<=0)//Condición durante el turbo, se acaba turbo
        {
            turbo=false;
            tick=0.5f;
            //Music//
            Assets.musicTurbo.stop();
            Assets.musicGame.play();
        }
    }
    /*****FIN TURBO****/

    //Cambia Background//
    public void cambiaBackg(int num)
    {
        switch(num){
            case 1:
                Assets.background= background2;
                break;
            case 2:
                Assets.background=Assets.background3;
                break;
        }


    }

    //Get score
    public String getScore()
    {
        return Integer.toString(score);
    }
}
